forward_feature_selection_rpart <- function(X, Y, max_features, 
                                            control=rpart.control(minsplit=41, maxdepth=14, cp=8e-5),
                                            verbose='TRUE'){
    size.CV<-floor(nrow(X)/5)
    selected<-NULL
    for (round in 1:max_features) { 
        candidates<-setdiff(1:ncol(X),selected)
        
        CV.err<-matrix(0,nrow=length(candidates),ncol=5)
        
        for (j in 1:length(candidates)) {
            features_to_include<-c(selected,candidates[j])
            
            for (i in 1:5) {
                i.ts<-(((i-1)*size.CV+1):(i*size.CV))  
                X.ts<-X[i.ts,features_to_include,drop=F]  
                Y.ts<-Y[i.ts]  
         
                i.tr<-setdiff(1:N,i.ts)
                X.tr<-X[i.tr,features_to_include,drop=F]
                Y.tr<-Y[i.tr]
         
                DS<-cbind(X.tr,price=Y.tr)
                model<- rpart(price~.,DS, control=control)
            
                Y.hat.ts<- predict(model,X.ts)
                neg <- Y.hat.ts[Y.hat.ts < 0]
                if (length(neg) > 0){
                    # print(neg)
                }
                CV.err[j,i]<- sqrt(mean((log(Y.hat.ts) - log(Y.ts))^2, na.rm=T))
            }
        }
        CV.err.mean<-apply(CV.err,1,mean)
        CV.err.sd<-apply(CV.err,1,sd)
        selected_current<-which.min(CV.err.mean)              
        selected<-c(selected,candidates[selected_current])
        if(verbose =='TRUE'){
            print(paste("Round ",round," ; Selected feature: ",candidates[selected_current]," ; CV error=",round(CV.err.mean[selected_current],digits=4), " ; std dev=",round(CV.err.sd[selected_current],digits=4)))
        }

    }
    return(selected)
}

bidirection_feature_selection_rpart <- function(X, Y, init_features, folds, max_rounds){
    features <- init_features
    size.CV<-floor(nrow(X)/folds)
    for (round in 1:max_rounds) { 
        # candidates<-setdiff(1:n,selected)
        candidates <- 1:ncol(X)
        
        CV.err<-matrix(0,nrow=length(candidates),ncol=10)

        for (j in 1:length(candidates)) {
            if (candidates[j] %in% features){
                features_to_include <- setdiff(features, candidates[j])
            }
            else {
                features_to_include<-c(features, candidates[j])
            }
            
            for (i in 1:folds) {
                i.ts<-(((i-1)*size.CV+1):(i*size.CV))  
                X.ts<-X[i.ts,features_to_include,drop=F]  
                Y.ts<-Y[i.ts]  
         
                i.tr<-setdiff(1:N,i.ts)
                X.tr<-X[i.tr,features_to_include,drop=F]
                Y.tr<-Y[i.tr]
         
                DS<-cbind(X.tr,price=Y.tr)
                model<- rpart(price~.,DS, )
            
                Y.hat.ts<- predict(model,X.ts)
                neg <- Y.hat.ts[Y.hat.ts < 0]
                if (length(neg) > 0){
                    # print(neg)
                }
                CV.err[j,i]<- sqrt(mean((log(Y.hat.ts) - log(Y.ts))^2, na.rm=T))
            }
        }
        CV.err.mean<-apply(CV.err,1,mean)
        CV.err.sd<-apply(CV.err,1,sd)
        selected_current<-which.min(CV.err.mean)              

        if (candidates[selected_current] %in% features){
            features <- setdiff(features, candidates[selected_current])
            print(paste("features ", length(features)," ; removed feature: ",candidates[selected_current]," ; CV error=",round(CV.err.mean[selected_current],digits=4), " ; std dev=",round(CV.err.sd[selected_current],digits=4)))
        }
        else{
            features <- c(features, candidates[selected_current])
            print(paste("features ", length(features)," ; added feature: ",candidates[selected_current]," ; CV error=",round(CV.err.mean[selected_current],digits=4), " ; std dev=",round(CV.err.sd[selected_current],digits=4)))
        }

    }
    return(features)
}

rpart_cross_validation <- function(X, Y, features, folds, control='F'){
    size.CV<-floor(nrow(X)/folds)
    error <- matrix(0, 1, folds)
    for (i in 1:folds) {
        i.ts<-(((i-1)*size.CV+1):(i*size.CV))  
        X.ts<-X[i.ts,features,drop=F]  
        Y.ts<-Y[i.ts]  
 
        i.tr<-setdiff(1:N,i.ts)
        X.tr<-X[i.tr,features,drop=F]
        Y.tr<-Y[i.tr]
 
        DS<-cbind(X.tr,price=Y.tr)
        # model<- rpart(price~.,DS, )
        if(control != 'F') {
            model<- rpart(price~., DS, method="anova", control=control )
        }
        else {
            model<- rpart(price~.,DS)
        }
    
        Y.hat.ts <- predict(model,X.ts)
        Y.hat.ts[Y.hat.ts < 0] <- median(Y.hat.ts)

        error[i] <- sqrt(mean((log(Y.hat.ts) - log(Y.ts))^2, na.rm=T))
    }
    return(list('error'=error, 'model'=model))
}

make_predictions <- function(X_tr, Y_tr, X_test, X_test_id, features_init, features_it, folds, model_qty){
    # raw_data <- read.csv(test_dataset)
    # test_data <- clean_data(raw_data)
    # X_test <- test_data[, features] 

    size.CV <- floor(nrow(X_tr)/model_qty)
    predictions <- matrix(0, nrow(X_test), model_qty)
    
    for (i in 1:model_qty) {
        features <- sample(1:ncol(X), features_init, replace=F)
        print(paste("model ", i))
        print(features)
        features <- multi_direction_feature_selection(X_tr, Y_tr, features, folds, features_it)

        i.ts <- (((i-1)*size.CV+1):(i*size.CV))  
 
        i.tr <- setdiff(1:N,i.ts)
        X_tr_i <- X_tr[i.tr,features,drop=F]
        Y_tr_i <- Y[i.tr]
 
        DS <-cbind(X_tr_i,SalePrice=Y_tr_i)
        model <- lm(SalePrice~.,DS, )
        
        X_test_i <- X_test[, features] 
    
        predictions[,i] <- predict(model,X_test_i)
    }
    Y_hat <- apply(predictions, 1, mean)
    return(data.frame(Id = X_test_id, SalePrice = Y_hat))
}

# Below are tests of no interest for the reader
# interesting_features <- c(26, 7, 10, 1, 14, 35, 15, 31, 6, 9, 38, 3, 11, 40, 12, 20, 24, 21, 43, 47, 8, 17)
# interesting_features <- c(5, 26, 15, 10, 40, 12, 20, 3, 21, 1, 6, 9, 38, 35, 36, 7, 31, 43, 22, 47, 34, 24)
# interesting_features <- c(5, 26, 15, 23, 33, 40, 7, 10, 43, 2, 6, 20, 38, 3, 47, 36, 52, 21, 27, 9, 13 , 11)
# # interesting_features <- interesting_features[1:15]
# 
# # Load raw data in 'data' variable
# library('rpart')
# data<-read.csv("train.csv")
# source('clean_data_functions.R')
# 
# data <- clean_data(data)
# set.seed(2)
# data <- data[sample(nrow(data), replace=F),]
# X<-data[,setdiff(colnames(data),"SalePrice")]
# Y<-data[,"SalePrice"]
# 
# raw_test_data <- read.csv('test.csv')
# X_test <- clean_data(raw_test_data)
# X_Id <- raw_test_data[,"Id"]
# 
# N<-nrow(X)    #Number of examples
# n<-ncol(X)    #Number of input variables
# 
# rm(list=".Random.seed", envir=globalenv()) 
# 
# X <- data.frame(prcomp(X,retx=T)$x)

# features <- sample(1:ncol(X), 5, replace=F)
# print(features)
# multi_direction_feature_selection_rpart(X, Y, features, 10, 10)



# features <- 1:ncol(X)
# features
# rpart_cross_validation(X, Y, features, 10)
