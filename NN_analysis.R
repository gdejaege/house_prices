forward_feature_selection_NN <- function(X, Y, folds, max_features, verbose=T, nodes=5, maxit=500, decay=0.3,
                                         init_features = NULL){
    X <- data.frame(scale(X))
    Y <- scale(Y)
    size.CV<-floor(N/max(folds,10))
    selected<-init_features
    for (round in 1:max_features) { 
        candidates <- setdiff(1:ncol(X), selected)
        
        CV.err<-matrix(0, nrow=length(candidates), ncol=folds)
        
        for (j in 1:length(candidates)) {
            features_to_include<-c(selected,candidates[j])
            
            set.seed(0)
            for (i in 1:folds) {
                i.ts<-(((i-1)*size.CV+1):(i*size.CV))  
                X.ts<-X[i.ts,features_to_include,drop=F]  
                Y.ts<-Y[i.ts]  
         
                i.tr<-setdiff(1:N,i.ts)
                X.tr<-X[i.tr,features_to_include,drop=F]
                Y.tr<-Y[i.tr]
         
                DS<-cbind(X.tr,price=Y.tr)
                model<- nnet(price~.,DS, size=nodes, maxit=maxit, decay=decay, linout=T, trace=F)
            
                Y.hat.ts <- (predict(model,X.ts)*attr(Y, 'scaled:scale') + attr(Y, 'scaled:center'))
                Y.hat.ts[Y.hat.ts < 0] <- median(Y.hat.ts)
                Y.ts <- Y.ts*attr(Y, 'scaled:scale') + attr(Y, 'scaled:center')

                CV.err[j,i]<- sqrt(mean((log(Y.hat.ts) - log(Y.ts))^2, na.rm=T))
            }
        }
        CV.err.mean<-apply(CV.err,1,mean)
        CV.err.sd<-apply(CV.err,1,sd)
        selected_current<-which.min(CV.err.mean)              
        selected<-c(selected,candidates[selected_current])
        if(verbose){
            print(paste("Round ",round," ; Selected feature: ", 
                        candidates[selected_current]," ; CV error=",
                        round(CV.err.mean[selected_current],digits=4), 
                        " ; std dev=",round(CV.err.sd[selected_current],
                                            digits=4)))
        }

    }
    return(selected)
}

forward_feature_selection_NN_with_log <- function(X, Y, folds, max_features, verbose=T, nodes=5, maxit=500, decay=0.3,
                                         init_features = NULL){
    X <- data.frame(scale(X))
    Y_log <- log10(Y)
    Y <- scale(Y_log)
    size.CV<-floor(N/max(folds,10))
    selected<-init_features
    for (round in 1:max_features) { 
        candidates <- setdiff(1:ncol(X), selected)
        
        CV.err<-matrix(0, nrow=length(candidates), ncol=folds)
        
        for (j in 1:length(candidates)) {
            features_to_include<-c(selected,candidates[j])
            
            set.seed(0)
            for (i in 1:folds) {
                i.ts<-(((i-1)*size.CV+1):(i*size.CV))  
                X.ts<-X[i.ts,features_to_include,drop=F]  
                Y.ts<-Y[i.ts]  
         
                i.tr<-setdiff(1:N,i.ts)
                X.tr<-X[i.tr,features_to_include,drop=F]
                Y.tr<-Y[i.tr]
         
                DS<-cbind(X.tr,price=Y.tr)
                model<- nnet(price~.,DS, size=nodes, maxit=maxit, decay=decay, linout=T, trace=F)
            
                Y.hat.ts <-10^(predict(model,X.ts)*attr(Y, 'scaled:scale') + attr(Y, 'scaled:center'))
                Y.hat.ts[Y.hat.ts < 0] <- median(Y.hat.ts)
                Y.ts <- 10^(Y.ts*attr(Y, 'scaled:scale') + attr(Y, 'scaled:center'))

                CV.err[j,i]<- sqrt(mean((log(Y.hat.ts) - log(Y.ts))^2, na.rm=T))
            }
        }
        CV.err.mean<-apply(CV.err,1,mean)
        CV.err.sd<-apply(CV.err,1,sd)
        selected_current<-which.min(CV.err.mean)              
        selected<-c(selected,candidates[selected_current])
        if(verbose){
            print(paste("Round ",round," ; Selected feature: ", 
                        candidates[selected_current]," ; CV error=",
                        round(CV.err.mean[selected_current],digits=4), 
                        " ; std dev=",round(CV.err.sd[selected_current],
                                            digits=4)))
        }

    }
    return(selected)
}
NN_cross_validation <- function(X, Y, features, folds, nodes=5, maxit=500, decay=0.8){
    X <- data.frame(scale(X))
    Y <- scale(Y)
    size.CV<-floor(nrow(X)/folds)
    error <- matrix(0, 1, folds)
    for (i in 1:folds) {
        i.ts<-(((i-1)*size.CV+1):(i*size.CV))  
        X.ts<-X[i.ts,features,drop=F]  
        Y.ts<-Y[i.ts]  
 
        i.tr<-setdiff(1:N,i.ts)
        X.tr<-X[i.tr,features,drop=F]
        Y.tr<-Y[i.tr]
 
        DS<-cbind(X.tr,price=Y.tr)
        model<- nnet(price~.,DS, size=nodes, maxit=maxit, decay=decay, linout=T, trace=F)
    
        Y.hat.ts <- (predict(model,X.ts)*attr(Y, 'scaled:scale') + attr(Y, 'scaled:center'))
        Y.hat.ts[Y.hat.ts < 0] <- median(Y.hat.ts)
        Y.ts <- Y.ts*attr(Y, 'scaled:scale') + attr(Y, 'scaled:center')

        error[i] <- sqrt(mean((log(Y.hat.ts) - log(Y.ts))^2, na.rm=T))
    }
    return(error)
}

NN_ensemble_predicition <- function(X, Y, X_test, features, models,
                                    nodes=5, maxit=500, decay=0.3, seed=0){
    X <- data.frame(scale(X[,features]))
    Y <- scale(Y)
    X_test <- data.frame(scale(X_test[,features]))

    predictions <- matrix(0, nrow(X_test), models)

    for (i in 1:models){
        indices <- sample(1:nrow(X), replace=T)
        X_it <- X[indices,]
        Y_it <- Y[indices]
        
        DS<-cbind(X_it, Price=Y_it)
        model<- nnet(Price~.,DS, size=nodes, maxit=maxit, decay=decay, linout=T, trace=F)

        predictions[,i] <- (predict(model, X_test) *attr(Y, 'scaled:scale') + attr(Y, 'scaled:center'))
    }

    Y_hat_final <- apply(predictions,1,mean)
    return(Y_hat_final)
}

NN_ensemble_predicition_with_log <- function(X, Y, X_test, features, models,
                                    nodes=5, maxit=500, decay=0.3, seed=0){
    X <- data.frame(scale(X[,features]))
    Y_log <- log10(Y)
    Y <- scale(Y_log)
    X_test <- data.frame(scale(X_test[,features]))

    predictions <- matrix(0, nrow(X_test), models)

    for (i in 1:models){
        indices <- sample(1:nrow(X), replace=T)
        X_it <- X[indices,]
        Y_it <- Y[indices]
        
        DS<-cbind(X_it, Price=Y_it)
        model<- nnet(Price~.,DS, size=nodes, maxit=maxit, decay=decay, linout=T, trace=F)

        predictions[,i] <- 10^(predict(model, X_test) *attr(Y, 'scaled:scale') + attr(Y, 'scaled:center'))
    }

    Y_hat_final <- apply(predictions,1,mean)
    return(Y_hat_final)
}


### Below are tests which are of no interest for the reader
# 
# library(nnet) 
# data<-read.csv("train.csv")
# set.seed(0)
# source('clean_data_functions.R')
# data <- clean_data(data)
# data <- data[sample(nrow(data), replace=F),]
# 
# X<-data[,setdiff(colnames(data),"SalePrice")]
# Y<-data[,"SalePrice"]
# 
# N<-nrow(X)    #Number of examples
# n<-ncol(X)    #Number of input variables
# 
# 
# folds <- 15
# max_features <- 35
# features <- c(5, 26, 40, 15, 20, 21, 28, 33)
# features <- c(5, 26, 15, 3, 13)
# features <- c(5, 26, 16, 33)
# features <- c(5, 26, 7, 20, 6, 3, 16, 40, 43, 33, 14, 32)
# features <- c(5, 26, 7, 20, 6, 3, 16, 40, 43, 33, 14, 32, 13, 37, 35, 39, 12, 23)
# # features <- c(5, 26, 7, 20, 6, 3, 16, 40, 43, 33, 14, 32, 13, 37, 35, 39, 12, 23, 25, 22, 17, 41)
# 
# raw_test_data <- read.csv('test.csv')
# X_test <- clean_data(raw_test_data)
# X_test_Id <- raw_test_data[,"Id"]     
# 
# models <- 20
# 
# predictions <- NN_ensemble_predicition(X, Y, X_test, features, models)
# 
# write.csv(data.frame(X_test_Id, predictions), 'submission_NN.csv', row.names=F)
# 
# 
# library(dummies)
# library(nnet) 
# 
# one_hot_var <- c("MSZoning", "Foundation" )
# 
# data<-read.csv("train.csv")
# set.seed(0)
# source('clean_data_functions.R')
# data <- clean_data_with_one_hot(data, one_hot_var)
# data <- clean_data(data)
# data <- data[sample(nrow(data), replace=F),]
# 
# 
# X<-data[,setdiff(colnames(data),"SalePrice")]
# Y<-data[,"SalePrice"]
# 
# N<-nrow(X)    #Number of examples
# n<-ncol(X)    #Number of input variables
# 
# raw_test_data <- read.csv('test.csv')
# X_test <- clean_data(raw_test_data)
# X_test_Id <- raw_test_data[,"Id"]
# 
# folds <- 15
# max_features <- 35
# features <- c(5, 26, 7, 20, 6, 3, 16, 40, 43, 33, 14, 32, 13)
# features <- c()
# features <- c(5, 26, 7, 20, 6, 3, 16, 40, 43, 33, 14, 32, 13, 37, 35, 39, 12, 23)
# models <- 15
# 
# forward_feature_selection_NN_with_log(X, Y, folds, max_features, verbose=T, nodes=5, maxit=500, decay=0.3, 
#                              init_features=features)
# 
# predictions <- NN_ensemble_predicition_with_log(X, Y, X_test, features, models)
# 
# write.csv(data.frame(X_test_Id, predictions), 'submission_NN_with_log.csv', row.names=F)
